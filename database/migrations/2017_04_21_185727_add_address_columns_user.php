<?php

use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\Schema;

class AddAddressColumnsUser extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('users', function ($table) {
				$table->string('cep', 12)->nullable();
				$table->string('endereco', 255)->nullable();
				$table->string('complemento', 100)->nullable();
				$table->integer('numero')->nullable();
				$table->string('cidade', 255)->nullable();
				$table->char('estado', 2)->nullable();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::create('users', function ($table) {
				$table->dropColumn('cep');
				$table->dropColumn('endereco');
				$table->dropColumn('complemento');
				$table->dropColumn('numero');
				$table->dropColumn('cidade');
				$table->dropColumn('estado');
			});
	}
}
