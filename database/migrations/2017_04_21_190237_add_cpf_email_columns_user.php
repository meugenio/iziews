<?php

use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\Schema;

class AddCpfEmailColumnsUser extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('users', function ($table) {
				$table->string('cpf', 20)->nullable();
				$table->string('email', 255)->nullable();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::create('users', function ($table) {
				$table->dropColumn('cpf');
				$table->dropColumn('email');
			});
	}
}
