<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller {

	protected $user = null;

	public function __construct(UserService $user) {
		$this->user = $user;
	}

	public function index() {
		$users = $this->user->getAll();

		if (!count($users)) {
			return response()->json(['error' => 'Não há registros'], 400);
		}

		return response()->json($users, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function save(Request $request) {
		$data = $request->all();

		if (!User::where('nome', $data['nome'])->count()) {
			$user = $this->user->createUser($data);

			return response()->json($user, 200);
		}

		return response()->json(['error' => 'Cliente já cadastrado'], 400);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$user    = $this->user->getUser($id);
		$address = $this->user->getAddressToUser($id);

		$user['addresses'] = $address;

		if (!$user) {
			return response()->json(['error' => 'Cliente não encontrado'], 400);
		}

		return response()->json($user, 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\User  $user
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Request $request) {
		$data = $request->all();

		$user = $this->user->updateUser($data);

		if (!$user) {
			return response()->json(['error' => 'Cliente não encontrado'], 400);
		}

		return response()->json($user, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  integer  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$user = $this->user->deleteUser($id);

		if (!$user) {
			return response()->json(['error' => 'Cliente não encontrado'], 400);
		}

		return response()->json(['response' => 'Cliente removido com sucesso'], 200);
	}
}
