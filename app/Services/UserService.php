<?php

namespace App\Services;

use App\Models\Address;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\DB;

class UserService {

	private $userRepository;

	public function __construct() {
	}

	public function setRepository(UserRepository $userRepository) {
		$this->userRepository = $userRepository;
	}

	public static function getAll() {
		return User::all();
	}

	public static function getAddressToUser($idUser) {
		$address = DB::table('addresses')->where('user_id', $idUser)->get();

		return $address;
	}

	private function convertDate($date) {
		return date("Y-m-d", strtotime($date));
	}

	public function createUser($data) {
		$data['nascimento'] = $this->convertDate($data['nascimento']);

		$user = new User();
		$user->fill($data);
		$user->save();

		$this->prepareAddress($data);

		return $user;
	}

	public function updateUser($data) {
		$id = $data['id'];

		$user = User::find($id);

		$this->prepareAddress($data, $user);

		$data['nascimento'] = $this->convertDate($data['nascimento']);

		if (is_null($user)) {
			return false;
		}

		$user->fill($data);
		$user->save();

		return $user;
	}

	private function removeAllAddress($user) {
		DB::table('addresses')->where('user_id', $user->id)->delete();
	}

	private function prepareAddress($data, $user) {
		if (isset($data['addresses']) && isset($data['address'])) {
			$this->removeAllAddress($user);

			if (count($data['address']) > 0) {
				foreach ($data['address'] as $value) {
					$value['id']      = null;
					$address          = new Address();
					$address->user_id = $user->id;
					$address->fill($value);
					$address->save();
				}
			}
		}
	}

	public function getUser($id) {
		$user = User::find($id);

		if (is_null($user)) {
			return false;
		}

		return $user;
	}

	public function deleteUser($id) {
		$user = User::find($id);

		if (is_null($user)) {
			return false;
		}

		return $user->delete();
	}

}