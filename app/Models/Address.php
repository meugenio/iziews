<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model {

	protected $fillable = [
		'cep',
		'endereco',
		'complemento',
		'numero',
		'estado',
		'municipio'
	];

	public function user() {
		return $this->hasMany('App\Models\User');
	}
}
