<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

	protected $fillable = [
		'nome',
		'nascimento',
		'sexo',
		'imagem',
		'cep',
		'cpf',
		'email'
	];

	public function address() {
		return $this->hasMany('App\Models\Address');
	}

}
