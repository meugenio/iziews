<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['prefix'   => 'v1'], function () {
		Route::group(['prefix' => 'user'], function () {

				Route::get('/', 'UserController@index');

				Route::get('{id}', 'UserController@show')->where('id', '[0-9]+');

				Route::post('', 'UserController@save');

				Route::put('', 'UserController@edit');

				Route::delete('{id}', 'UserController@destroy')->where('id', '[0-9]+');

			});
	});